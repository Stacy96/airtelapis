package com.example.springairtel.api;

import org.apache.commons.collections.MapIterator;
import org.apache.commons.collections.map.LRUMap;

import java.util.ArrayList;

public class CacheVariables <K, T>{
    private final long timeTolive;
    private final LRUMap CacheMap;

    protected class CacheObject{
        long lastAccessed = System.currentTimeMillis();
        T value;

        protected CacheObject (T value){
            this.value = value;
        }
    }
    public CacheVariables(long TimeToLive, final long TimerInterval, int maxItems) {
        this.timeTolive = TimeToLive * 1000;

        CacheMap = new LRUMap(maxItems);

        if (timeTolive > 0 && TimerInterval > 0) {

            Thread t = new Thread(new Runnable() {
                public void run() {
                    while (true) {
                        try {

                            // Thread: A thread is a thread of execution in a program.
                            // The Java Virtual Machine allows an application to have multiple threads of execution running concurrently.
                            Thread.sleep(TimerInterval * 1000);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                        Cleanup();
                    }
                }
            });
            // setDaemon(): Marks this thread as either a daemon thread or a user thread.
            // The Java Virtual Machine exits when the only threads running are all daemon threads.
            // This method must be invoked before the thread is started.
            t.setDaemon(true);

            // start(): Causes this thread to begin execution; the Java Virtual Machine calls the run method of this thread.
            // The result is that two threads are running concurrently:
            // the current thread (which returns from the call to the start method) and the other thread (which executes its run method).
            t.start();
        }
    }
    public void put(K key, T value) {
        synchronized (CacheMap) {

            // put(): Puts a key-value mapping into this map.
            CacheMap.put(key, new CacheObject(value));
        }
    }

    public T get(K key) {
        synchronized (CacheMap) {
            CacheObject c;
            c = (CacheObject) CacheMap.get(key);

            if (c == null)
                return null;
            else {
                c.lastAccessed = System.currentTimeMillis();
                return c.value;
            }
        }
    }

    public void remove(K key) {
        synchronized (CacheMap) {
            CacheMap.remove(key);
        }
    }

    public int size() {
        synchronized (CacheMap) {
            return CacheMap.size();
        }
    }

    public void Cleanup() {

        // System: The System class contains several useful class fields and methods.
        // It cannot be instantiated. Among the facilities provided by the System class are standard input, standard output,
        // and error output streams; access to externally defined properties and environment variables;
        // a means of loading files and libraries; and a utility method for quickly copying a portion of an array.
        long now = System.currentTimeMillis();
        ArrayList<K> deleteKey = null;

        synchronized (CacheMap) {
            MapIterator itr = CacheMap.mapIterator();

            // ArrayList: Constructs an empty list with the specified initial capacity.
            // size(): Gets the size of the map.
            deleteKey = new ArrayList<K>((CacheMap.size() / 2) + 1);
            K key = null;
            CacheObject c = null;

            while (itr.hasNext()) {
                key = (K) itr.next();
                c = (CacheObject) itr.getValue();

                if (c != null && (now > (timeTolive + c.lastAccessed))) {

                    // add(): Appends the specified element to the end of this list.
                    deleteKey.add(key);
                }
            }
        }

        for (K key : deleteKey) {
            synchronized (CacheMap) {

                // remove(): Removes the specified mapping from this map.
                CacheMap.remove(key);
            }

            // yield(): A hint to the scheduler that the current thread is willing to
            // yield its current use of a processor.
            // The scheduler is free to ignore this hint.
            Thread.yield();
        }
    }
}
